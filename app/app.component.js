System.register(['angular2/core', './product.service', './basket.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, product_service_1, basket_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (product_service_1_1) {
                product_service_1 = product_service_1_1;
            },
            function (basket_component_1_1) {
                basket_component_1 = basket_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(productService) {
                    this.productService = productService;
                }
                AppComponent.prototype.ngOnInit = function () {
                    this._products = this.productService.getProducts();
                };
                AppComponent.prototype.onProductSelectedForBuy = function (selectedProduct) {
                    this.basketComponent.buy(selectedProduct);
                };
                __decorate([
                    core_1.ViewChild(basket_component_1.BasketComponent), 
                    __metadata('design:type', basket_component_1.BasketComponent)
                ], AppComponent.prototype, "basketComponent", void 0);
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "<h1>Simple product buying demo with components iteractions</h1>\n            <div style='float: right; border: 3px solid'>\n                <basket></basket>\n            </div>\n            <ul>\n                <li *ngFor=\"#product of _products\">\n                    <div>{{product.name}} for {{product.price}}$ <button (click)=\"onProductSelectedForBuy(product)\">BUY</button></div>\n                    <img src={{product.image}} />\n                </li>\n            </ul>           \n    ",
                        providers: [product_service_1.ProductService],
                        directives: [basket_component_1.BasketComponent]
                    }), 
                    __metadata('design:paramtypes', [product_service_1.ProductService])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map