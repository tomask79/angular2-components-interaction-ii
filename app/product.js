System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ProductType;
    return {
        setters:[],
        execute: function() {
            ProductType = (function () {
                function ProductType(name, price, image) {
                    this.name = name;
                    this.price = price;
                    this.image = image;
                }
                return ProductType;
            }());
            exports_1("ProductType", ProductType);
        }
    }
});
//# sourceMappingURL=product.js.map