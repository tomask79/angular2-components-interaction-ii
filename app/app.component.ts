import {Component, ViewChild, OnInit } from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {BasketComponent} from './basket.component';

@Component({
    selector: 'my-app',
    template: `<h1>Simple product buying demo with components iteractions</h1>
            <div style='float: right; border: 3px solid'>
                <basket></basket>
            </div>
            <ul>
                <li *ngFor="#product of _products">
                    <div>{{product.name}} for {{product.price}}$ <button (click)="onProductSelectedForBuy(product)">BUY</button></div>
                    <img src={{product.image}} />
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [BasketComponent]
})
export class AppComponent implements OnInit  {
     private _products: ProductType[];

     @ViewChild(BasketComponent)
     private basketComponent:BasketComponent;

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }

    onProductSelectedForBuy(selectedProduct: ProductType) {
        this.basketComponent.buy(selectedProduct);
    }
}