# Writing applications with Angular2, [part4] #

In my previous post I named list of most used ways of [how Angular 2 components can interact](https://bitbucket.org/tomask79/angular2-components-interaction/overview) with each other. We successfully tested component interaction via **local variable**. But using of this technique is limited, 
because **local variable can be used only in the template**. But what about situations when we want to access child components API outside of template?
Well, to solve this problem Angular team has provided us component interaction technique called **"ViewChild Injection"**. How does it work?

## Parent calls a ViewChild ##

### Principle ###

This technique is based on the injection of child component into parent component via Angular2 decorator called **ViewChild**. 
If we inject child component into parent component like this as a field then local variable is not longer necessary. 
Let's change previous app example with product list and simple basket to use this technique:

**Parent component:**

```
import {Component, ViewChild, OnInit } from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {BasketComponent} from './basket.component';

@Component({
    selector: 'my-app',
    template: `<h1>Simple product buying demo with components iteractions</h1>
            <div style='float: right; border: 3px solid'>
                <basket></basket>
            </div>
            <ul>
                <li *ngFor="#product of _products">
                    <div>{{product.name}} for {{product.price}}$ <button (click)="onProductSelectedForBuy(product)">BUY</button></div>
                    <img src={{product.image}} />
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [BasketComponent]
})
export class AppComponent implements OnInit  {
     private _products: ProductType[];

     @ViewChild(BasketComponent)
     private basketComponent:BasketComponent;

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }

    onProductSelectedForBuy(selectedProduct: ProductType) {
        this.basketComponent.buy(selectedProduct);
    }
}
```
* Notice how we call method **onProductSelectedForBuy** and send selected product into it.
* Notice how we send selected product to buy into basket without local variable and outside of a template and that's the point of this technique!
* Other parts of app remains the same.

## Testing the demo ##

* git clone <this repo>
* npm install (in the angular2-components-interaction-ii directory)
* npm start
* Visit http://localhost:3000

App should be working as in the previous post but with different technique.



 